const dc = require('discord.js');
const fs = require('fs');

const config = JSON.parse(fs.readFileSync('config.json'), 'utf 8');

var client = new dc.Client();

var block = false;

client.login(config.token);

client.on('ready', () => {
    console.log(`SkyBot started!`);
  });


//cmd
client.on('message', msg => {
    if(msg.content.startsWith(config.prefix)) {
        console.log(`${msg.author.displayName}: ${msg.content}`)
        var commandAray = msg.content.split(' ');
        var command = commandAray[1];
        var args = commandAray.slice(2);
        if(command == 'switchblock') {
            if(msg.author.id == config.devID) {
                if(block == true) {
                    msg.channel.send('block disabled')
                    block = false;
                } else {
                    msg.channel.send('block enabled')
                    block = true;
                }
            } else {
                msg.channel.send('you are not allowed to do this!')
            }
        }
        if(block == true ) {
            msg.channel.send('the bot is currently in block mode.')
        } else {
        if(command == 'say') {
            msg.channel.send(args.join(' '));
        }
        if(command == 'shutdown') {
            if(msg.author.id == config.devID) {
                msg.channel.send('Bye Guys!');
                console.log('shutdown')
                client.destroy()
            } else {
                msg.channel.send('you are not allowed to do this!')
            } 
        }
        if(command == 'cname') {
            if(msg.author.id == config.devID) {
                client.user.setUsername(args[0]);
            } else {
                msg.channel.send('you are not allowed to do this!')
                } 
            }
        if(command == 'ban') {
            if(args[0] != null) {
                if(msg.guild.members.find("id",msg.author.id).hasPermission("ADMINISTRATOR")) {
                    banme = msg.guild.members.find('name',args[0]);
                    msg.guild.ban(banme)
                } else {
                    msg.channel.send('You don\'t have the permissions to do that!');
                }
            }
        }
        }
    }
})

//ban -- log
client.on('guildBanAdd', member => {
    const lchannel = member.guild.channels.find('name','log');
    if(!lchannel) return;
    const mname = member.displayName;
    lchannel.send(`${mname} was banned from this server.`)
})
client.on('guildBanRemove', member => {
    const lchannel = member.guild.channels.find('name','log');
    if(!lchannel) return;
    const mname = member.displayName;
    lchannel.send(`${mname} is unbanned from this server.`)
})
//join/leave -- log
client.on('guildMemberAdd', member => {
    const lchannel = member.guild.channels.find('name','log');
    if(!lchannel) return;
    const mname = member.displayName;
    lchannel.send(`${mname} joined the server.`)
})
client.on('guildMemberRemove', member => {
    const lchannel = member.guild.channels.find('name','log');
    if(!lchannel) return;
    const mname = member.displayName;
    lchannel.send(`${mname} left the server.`)
})

//channels -- log
client.on('channelCreate', channel => {
    const lchannel = channel.guild.channels.find('name','log')
    if(!channel) return;
    const channelname = channel.id;
    lchannel.send(`New Channel: ${channelname}`)
})
client.on('channelDelete', channel => {
    const lchannel = channel.guild.channels.find('name','log')
    if(!channel) return;
    var channelname = channel.id
    lchannel.send(`Channel removed: ${channelname}`)
})
client.on('channelUpdate', channel => {
    const lchannel = channel.guild.channels.find('name','log')
    if(!channel) return;
    var channelname = channel.id
    lchannel.send(`Channel edited: ${channelname}`)
})



client.on('reconnecting', () => {
    console.log('reconnected.')
})
